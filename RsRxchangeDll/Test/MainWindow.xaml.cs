﻿using System;
using System.Windows;
using RsRxchangeDll;
using ModelsTablesDBLib;
using System.Collections.Generic;
using System.Threading;
using RsRxchangeDll.Serialization;
using ModelsTablesDBLib.Interfaces;

namespace Test
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            device = new RSDevice(new ModelSerialization());
            device.OnConnect += Device_OnConnect;
            device.OnDisconnect += Device_OnDisconnect;
            (device.DictCommands[Requests.TextMess] as IEventRequest<string>).OnRequest += TextMess_Request;
            (device.DictCommands[Requests.TextMess] as IEventConfirm).OnConfirm += TextMess_Confirm;
            (device.DictCommands[Requests.TableAsp] as IEventRequest<List<IFixASP>>).OnRequest += TableAsp_Request;
            (device.DictCommands[Requests.TableAsp] as IEventConfirm).OnConfirm += TableAsp_Confirm;
            (device.DictCommands[Requests.QuasiDirectFind] as IEventRequest<IQuasiRequest>).OnRequest += MainWindow_OnRequest;
            (device.DictCommands[Requests.QuasiDirectFind] as IEventConfirm).OnConfirm += MainWindow_OnConfirm;
            (device.DictCommands[Requests.Regime] as IEventRequest<byte>).OnRequest += MainWindow_OnRequest1;
            (device.DictCommands[Requests.Synchronize] as IEventConfirm).OnConfirm += MainWindow_OnConfirm1;
            (device.DictCommands[Requests.Synchronize] as IEventRequest<DateTime>).OnRequest += MainWindow_OnRequest2;
        }

        private void MainWindow_OnRequest2(object sender, DateTime e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                BoxLog.AppendText($"{DateTime.Now.ToShortTimeString()} REQUEST: SYNCHRONIZE: Time = {e.ToShortTimeString()} \n");
            });
        }

        private void MainWindow_OnConfirm1(object sender, ModelConfirm e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                BoxLog.AppendText($"{DateTime.Now.ToShortTimeString()} CONFIRM: SYNCHRONIZE: Time = {(e as ModelConfirm<DateTime>).Model.ToShortTimeString()} \n");
            });
        }

        private void MainWindow_OnRequest1(object sender, byte e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                BoxLog.AppendText($"{DateTime.Now.ToShortTimeString()} REQUEST: REGIME: {e} \n");
            });
        }

        private void MainWindow_OnConfirm(object sender, IConfirm e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                BoxLog.AppendText($"{DateTime.Now.ToShortTimeString()} CONFIRM: QUASI Bearing {(e as IConfirm<IQuasiConfirm>)?.Model?.Bearing} \n");
            });
        }

        private void MainWindow_OnRequest(object sender, IQuasiRequest e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                BoxLog.AppendText($"{DateTime.Now.ToShortTimeString()} REQUEST: QUASI  Freq = {e.Frequency}  Time = {e.Time.ToShortTimeString()}\n");
            });
        }

        private void TextMess_Confirm(object sender, IConfirm e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                BoxLog.AppendText($"{DateTime.Now.ToShortTimeString()} CONFIRM: TEXT ErrorCode {e.CodeError} \n");
            });
        }

        private void TextMess_Request(object sender, string e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                BoxLog.AppendText($"{DateTime.Now.ToShortTimeString()} REQUEST: TEXT  {e} \n");
            });
        }

        private void TableAsp_Confirm(object sender, IConfirm e)
        { 
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    BoxLog.AppendText($"{DateTime.Now.ToShortTimeString()} CONFIRM: Table ASP {(e as IConfirm<List<IFixASP>>)?.Model?.Count} \n");
                });
        }

        private void TableAsp_Request(object sender, List<IFixASP> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                BoxLog.AppendText($"{DateTime.Now.ToShortTimeString()} REQUEST: Table ASP {e.Count} \n");
            });
        }
        
        private void Device_OnDisconnect(object sender, EventArgs e)
        {
            Connect.Content = "Connect";
            Disconnect.Content = "Disconnected!!!";
        }

        private void Device_OnConnect(object sender, EventArgs e)
        {
            Connect.Content = "Connected!!!";
        }

        RSDevice device;
        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            device.Connect((string)ComPors.SelectedItem);
        }

        private void Disconnect_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Request_Click(object sender, RoutedEventArgs e)
        {
            Random rand = new Random();
            switch (RequestNumber.SelectedItem)
            {
                case Requests.TableAsp:
                    List<IFixASP> model = new List<IFixASP>()
                {
                    new TableASP
                        {
                            Id = 1,
                            IdMission = 1,
                            ISOwn = true,
                            CallSign = "ASP",
                            IsConnect = (Led)rand.Next(0, 3),
                            Letters = new byte[] { 0, 0, 0, 0, 1, 1, 1, 1, 1, 0 },
                            Coordinates = new Coord()
                            {
                                Altitude = rand.Next(0,360),
                                Latitude = rand.Next(0,360),
                                Longitude = rand.Next(0,360)
                            },
                            LPA13 = (short)rand.Next(0, 360),
                            LPA24 = (short)rand.Next(0, 360),
                            LPA510 = (short)rand.Next(0, 360),
                            RRS1 = (short)rand.Next(0, 360),
                            RRS2 = (short)rand.Next(0, 360),
                            Mode = (byte)rand.Next(0, 255)
                        },
                    new TableASP
                        {
                            Id = 2,
                            IdMission = 2,
                            ISOwn = true,
                            CallSign = "ASP",
                            IsConnect = (Led)rand.Next(0, 3),
                            Letters = new byte[] { 0, 0, 0, 0, 1, 1, 1, 1, 1, 0 },
                            Coordinates = new Coord()
                            {
                                Altitude = rand.Next(0,360),
                                Latitude = rand.Next(0,360),
                                Longitude = rand.Next(0,360)
                            },
                            LPA13 = (short)rand.Next(0, 360),
                            LPA24 = (short)rand.Next(0, 360),
                            LPA510 = (short)rand.Next(0, 360),
                            RRS1 = (short)rand.Next(0, 360),
                            RRS2 = (short)rand.Next(0, 360),
                            Mode = (byte)rand.Next(0, 255)
                        },
                    new TableASP
                        {
                            Id = 3,
                            IdMission = 3,
                            ISOwn = true,
                            CallSign = "ASP",
                            IsConnect = (Led)rand.Next(0, 3),
                            Letters = new byte[] { 0, 0, 0, 0, 1, 1, 1, 1, 1, 0 },
                            Coordinates = new Coord()
                            {
                                Altitude = rand.Next(0,360),
                                Latitude = rand.Next(0,360),
                                Longitude = rand.Next(0,360)
                            },
                            LPA13 = (short)rand.Next(0, 360),
                            LPA24 = (short)rand.Next(0, 360),
                            LPA510 = (short)rand.Next(0, 360),
                            RRS1 = (short)rand.Next(0, 360),
                            RRS2 = (short)rand.Next(0, 360),
                            Mode = (byte)rand.Next(0, 255)
                        },
                    };
                    device.DictCommands[(Requests)RequestNumber.SelectedItem].SendRequest(1, 1, model);
                    break;
                case Requests.QuasiDirectFind:
                    device.DictCommands[(Requests)RequestNumber.SelectedItem].SendRequest(1, 1, new QuasiReq() { Frequency = rand.Next(300000, 60000000), Time = DateTime.Now });
                    break;
                case Requests.Regime:
                    device.DictCommands[(Requests)RequestNumber.SelectedItem].SendRequest(1, 1, (byte)rand.Next(0,3));
                    break;
                case Requests.Synchronize:
                    device.DictCommands[(Requests)RequestNumber.SelectedItem].SendRequest(1, 1, DateTime.Now);
                    break;
                case Requests.TableFreqForbidden:
                    break;
                case Requests.TableFreqImportant:
                    break;
                case Requests.TableFreqKnown:
                    break;
                case Requests.TableReconFWS:
                    break;
                case Requests.TableSectorRangeRecon:
                    break;
                case Requests.TableSectorRangeSuppr:
                    break;
                case Requests.TableSuppressFHSS:
                    break;
                case Requests.TableSuppressFWS:
                    break;
                case Requests.TempSuppressFHSS:
                    break;
                case Requests.TempSuppressFWS:
                    break;
                case Requests.TextMess:
                    string Message = BoxTextMess.Text;
                    device.DictCommands[(Requests)RequestNumber.SelectedItem].SendRequest(1, 1, Message);
                    break;                
            }
        }

        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            Random rand = new Random();

            switch (RequestNumber.SelectedItem)
            {
                case Requests.TableAsp:
                    List<IFixASP> model = new List<IFixASP>()
                {
                    new TableASP
                        {
                            Id = 1,
                            IdMission = 1,
                            ISOwn = true,
                            CallSign = "ASP",
                            IsConnect = (Led)rand.Next(0, 3),
                            Letters = new byte[] { 0, 0, 0, 0, 1, 1, 1, 1, 1, 0 },
                            Coordinates = new Coord()
                            {
                                Altitude = rand.Next(0,360),
                                Latitude = rand.Next(0,360),
                                Longitude = rand.Next(0,360)
                            },
                            LPA13 = (short)rand.Next(0, 360),
                            LPA24 = (short)rand.Next(0, 360),
                            LPA510 = (short)rand.Next(0, 360),
                            RRS1 = (short)rand.Next(0, 360),
                            RRS2 = (short)rand.Next(0, 360),
                            Mode = (byte)rand.Next(0, 255)
                        },
                    new TableASP
                        {
                            Id = 2,
                            IdMission = 2,
                            ISOwn = true,
                            CallSign = "ASP",
                            IsConnect = (Led)rand.Next(0, 3),
                            Letters = new byte[] { 0, 0, 0, 0, 1, 1, 1, 1, 1, 0 },
                            Coordinates = new Coord()
                            {
                                Altitude = rand.Next(0,360),
                                Latitude = rand.Next(0,360),
                                Longitude = rand.Next(0,360)
                            },
                            LPA13 = (short)rand.Next(0, 360),
                            LPA24 = (short)rand.Next(0, 360),
                            LPA510 = (short)rand.Next(0, 360),
                            RRS1 = (short)rand.Next(0, 360),
                            RRS2 = (short)rand.Next(0, 360),
                            Mode = (byte)rand.Next(0, 255)
                        },
                    new TableASP
                        {
                            Id = 3,
                            IdMission = 3,
                            ISOwn = true,
                            CallSign = "ASP",
                            IsConnect = (Led)rand.Next(0, 3),
                            Letters = new byte[] { 0, 0, 0, 0, 1, 1, 1, 1, 1, 0 },
                            Coordinates = new Coord()
                            {
                                Altitude = rand.Next(0,360),
                                Latitude = rand.Next(0,360),
                                Longitude = rand.Next(0,360)
                            },
                            LPA13 = (short)rand.Next(0, 360),
                            LPA24 = (short)rand.Next(0, 360),
                            LPA510 = (short)rand.Next(0, 360),
                            RRS1 = (short)rand.Next(0, 360),
                            RRS2 = (short)rand.Next(0, 360),
                            Mode = (byte)rand.Next(0, 255)
                        },
                    };
                    device.DictCommands[(Requests)RequestNumber.SelectedItem].SendConfirm(1, 1, new ModelConfirm<List<IFixASP>>() { CodeError = 0, Model = model });
                    break;
                case Requests.QuasiDirectFind:
                    device.DictCommands[(Requests)RequestNumber.SelectedItem].SendConfirm(1, 1, new ModelConfirm<IQuasiConfirm>() { CodeError = 0, Model = new QuasiConf() { Bearing = (short)rand.Next(0, 359) } });
                    
                    break;
                case Requests.Regime:
                    device.DictCommands[(Requests)RequestNumber.SelectedItem].SendConfirm(1, 1, new ModelConfirm() { CodeError = 12} );
                    break;
                case Requests.Synchronize:
                    device.DictCommands[(Requests)RequestNumber.SelectedItem].SendConfirm(1, 1, new ModelConfirm<DateTime>() { CodeError = 0, Model = DateTime.Now });
                    break;
                case Requests.TableFreqForbidden:
                    break;
                case Requests.TableFreqImportant:
                    break;
                case Requests.TableFreqKnown:
                    break;
                case Requests.TableReconFWS:
                    break;
                case Requests.TableSectorRangeRecon:
                    break;
                case Requests.TableSectorRangeSuppr:
                    break;
                case Requests.TableSuppressFHSS:
                    break;
                case Requests.TableSuppressFWS:
                    break;
                case Requests.TempSuppressFHSS:
                    break;
                case Requests.TempSuppressFWS:
                    break;
                case Requests.TextMess:
                    string Message = BoxTextMess.Text;
                    device.DictCommands[(Requests)RequestNumber.SelectedItem].SendConfirm(1, 1, new ModelConfirm() { CodeError = 12});
                    break;
            }
        }
    }
}
