﻿using System;
using System.Collections.Generic;
using ModelsTablesDBLib.Interfaces;
using RsRxchangeDll.Serialization;

namespace RsRxchangeDll
{
    public class RSDevice : IExchange
    {
        #region Events

        public event EventHandler OnConnect = (sender, args) => { };

        public event EventHandler OnDisconnect = (sender, args) => { };

        #endregion

        #region Private

        private ComPortOperation Port;

        private string ComPortName;

        private int SpeedComPort;

        private Parser Parser;

        #endregion

        #region Readonly

        public readonly Dictionary<Requests, ICommands> DictCommands;        

        #endregion

        public RSDevice(ISerialization Serializer)
        {
            Parser = new Parser(Serializer);
            Parser.OnSerializedModelMess += SendSerializedData;
            Port = new ComPortOperation();
            Port.OnDisconnect += Handler_OnDisconnect;
            Port.OnReadMessage += Handler_OnReadMessage;
            ComPortName = "COM1";
            SpeedComPort = 115200;
            DictCommands = new Dictionary<Requests, ICommands>()
            {
                {Requests.TextMess, new ParserTextMessage() },
                {Requests.TableAsp, new ParserListMessages<ModelASP,IFixASP>(Requests.TableAsp) },
                {Requests.Synchronize, new ParserSynchronize() },
                {Requests.TableSectorRangeRecon, new ParserListMessages<ModelSectorRanges, IFixSectorRanges>(Requests.TableSectorRangeRecon)},
                {Requests.TableSectorRangeSuppr, new ParserListMessages<ModelSectorRanges, IFixSectorRanges>(Requests.TableSectorRangeSuppr)},
                {Requests.TableFreqForbidden, new ParserListMessages<ModelSpecFreq, IFixSpecFreq>(Requests.TableFreqForbidden)},
                {Requests.TableFreqKnown, new ParserListMessages<ModelSpecFreq, IFixSpecFreq>(Requests.TableFreqKnown)},
                {Requests.TableFreqImportant, new ParserListMessages<ModelSpecFreq, IFixSpecFreq>(Requests.TableFreqImportant)},
                {Requests.TableSuppressFWS, new ParserListMessages<ModelSupprFWS, IFixSupprFWS>(Requests.TableSuppressFWS) },
                {Requests.TableSuppressFHSS, new ParserListMessages<ModelSupprFHSS, IFixSupprFHSS>(Requests.TableSuppressFHSS) },
                {Requests.Regime, new ParserRegime() },
                {Requests.TableReconFWS, new ParserListMessages<ModelReconFWS, IFixReconFWS>(Requests.TableReconFWS) },
                {Requests.TempSuppressFWS, new ParserListMessages<ModelTempSuppr, IFixTempSuppr>(Requests.TempSuppressFWS) },
                {Requests.TempSuppressFHSS, new ParserListMessages<ModelTempSuppr, IFixTempSuppr>(Requests.TempSuppressFHSS) },
                {Requests.QuasiDirectFind, new ParserQuasi() },
                {Requests.Ping, new PingParser()}
            };
        }

        private void SendSerializedData(object sender, byte[] data)
        {
            try
            {
                Port.WriteData(data);
            }
            catch (Exception)
            {

            }
        }

        private void Handler_OnReadMessage(object sender, ModelMessage e)
        {
            if (e.SignMessage == ModelMessage.SignRequest)
            {
                var Model = (DictCommands[Common.Codes[e.CodeMessage]] as IDeserializeMess).TryDeserializeRequest(e.Data);
            }
            else
            {
                var Model = (DictCommands[Common.Codes[e.CodeMessage]] as IDeserializeMess).TryDeserializeConfirm(e.Data);
            }
        }

        public RSDevice(string ComPortName, int SpeedComPort, ISerialization Serializer) :this(Serializer)
        {
            this.ComPortName = ComPortName;
            this.SpeedComPort = SpeedComPort;
        }
        
        private void Handler_OnDisconnect(object sender, EventArgs e)
        {
            OnDisconnect(this, null);
        }

        private void Port_OnConnect(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void Connect(string ComPortName)
        {
            this.ComPortName = ComPortName;
            if (Port.OpenPort(ComPortName))
                OnConnect(this, null);
            else
                OnDisconnect(this, null);
        }

        public void Connect(string ComPortName, int rate)
        {
            this.ComPortName = ComPortName;
            if (Port.OpenPort(ComPortName, rate))
                OnConnect(this, null);
            else
                OnDisconnect(this, null);
        }

        public void Disconnect()
        {
            Port.ClosePort();
        }
    }
}
