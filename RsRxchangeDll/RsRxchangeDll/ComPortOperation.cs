﻿using System;
using System.IO.Ports;
using System.Threading;

namespace RsRxchangeDll
{
    internal class ComPortOperation : IComOperation
    {
        #region Events
        
        public event EventHandler OnDisconnect = (obj, args) => { };

        public event EventHandler<ModelMessage> OnReadMessage = (obj, args) => { };

        #endregion

        #region Private

        private SerialPort serialPort;

        private Thread thrRead;

        private Parity defaultParity = Parity.None;

        private int dafaultDataBits = 8;

        private StopBits defaultStopBits = StopBits.One;

        private int defaultSpeed = 9600;

        #endregion

        public bool OpenPort(string ComPortName, int speed, Parity parity, int dataBits, StopBits stopBits)
        {
            if (serialPort == null)
                serialPort = new SerialPort();

            if (serialPort.IsOpen)
                ClosePort();
            try
            {
                // set parameters of port
                serialPort.PortName = ComPortName;
                serialPort.BaudRate = speed;

                serialPort.Parity = parity;
                serialPort.DataBits = dataBits;
                serialPort.StopBits = stopBits;

                serialPort.RtsEnable = true;
                serialPort.DtrEnable = true;


                serialPort.ReceivedBytesThreshold = 1000;

                serialPort.Open();

                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // load function of the thread for reading data from the port
                try
                {
                    thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }
                catch (System.Exception)
                {

                }
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        public bool OpenPort(string ComPortName, int speed)
        {
           return OpenPort(ComPortName, speed, defaultParity, dafaultDataBits, defaultStopBits);
        }

        public bool OpenPort(string ComPortName)
        {
           return OpenPort(ComPortName, defaultSpeed, defaultParity, dafaultDataBits, defaultStopBits);
        }

        public void ClosePort()
        {
            try
            {
                serialPort.DiscardInBuffer();
            }
            catch (Exception ex)
            {
                ex.GetBaseException();
            }

            try
            {
                serialPort.DiscardOutBuffer();
            }
            catch (Exception ex)
            {
                ex.GetBaseException();
            }

            try
            {
                serialPort.Close();

                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                OnDisconnect(this, null);
            }
            catch (Exception ex)
            {
                ex.GetBaseException();
            }
        }

        public void WriteData(byte[] data)
        {
            try
            {
                serialPort.Write(data, 0, data.Length);
            }
            catch (Exception)
            {
                OnDisconnect(this, null);
            }
        }
        
        private void ReadData()
        {
            byte[] buffer;
            int iReadByte;

            byte[] bufferSave = null;

            int iBufferSave = 0;

            while (true)
            {
                try
                {
                    buffer = new byte[4000000];                   

                    iReadByte = serialPort.Read(buffer, 0, buffer.Length);

                    if (iReadByte > 0)
                    {
                        Array.Resize(ref buffer, iReadByte);

                        if (bufferSave == null)
                            bufferSave = new byte[iReadByte];
                        else
                            Array.Resize(ref bufferSave, bufferSave.Length + iReadByte);


                        Array.Copy(buffer, 0, bufferSave, bufferSave.Length - iReadByte, iReadByte);
                        iBufferSave += iReadByte;

                        while (iBufferSave >= ModelMessage.LengthServicePart)
                        {
                           ModelMessage message = TrySerializeData(ref bufferSave, ref iBufferSave);
                            if (message != null)
                                OnReadMessage(this, message);
                            else
                                break;
                        }
                    }

                    else
                        OnDisconnect(this, null);
                }

                catch (Exception)
                {

                    ClosePort();

                }

                Thread.Sleep(5);
            }
        }

        private ModelMessage TrySerializeData(ref byte[] bufferSave, ref int iBufferSave)
        {
            try
            {
                ModelMessage message = ModelMessage.TryParseServicePart(bufferSave);
                if (message == null)
                {
                    Array.Resize(ref bufferSave, 0);
                    iBufferSave = 0;
                    return null;
                }

                if (iBufferSave >= ModelMessage.LengthServicePart + message.Length)
                {
                    message.Data = new byte[message.Length];

                    Array.Copy(bufferSave, ModelMessage.LengthServicePart, message.Data, 0, message.Length);

                    Array.Reverse(bufferSave);
                    Array.Resize(ref bufferSave, bufferSave.Length - (ModelMessage.LengthServicePart + message.Length));
                    Array.Reverse(bufferSave);

                    iBufferSave -= ModelMessage.LengthServicePart + message.Length;
                    return message;
                }
                return null;

            }
            catch (Exception e)
            {
                Array.Resize(ref bufferSave, 0);
                iBufferSave = 0;
                return null;
            }
        }

    }
}
