﻿using System;

namespace RsRxchangeDll
{
    [ProtoBuf.ProtoContract]
    internal class ModelMessage: IServicePart
    {
        #region Property

        public short AddressSender { get; private set; }

        public short AddressReceiver { get; private set; }

        public byte CodeMessage { get; private set; }

        public byte SignMessage { get; private set; } // 0 - Request, 1-Confirm

        public byte ReservePart { get; private set; }

        public int Length { get; private set; }

        public byte[] Data { get; set; }

        #endregion

        #region Const

        public  const int LengthServicePart = 11;

        public const byte SignRequest = 0;

        public const byte SignConfirm = 1;

        #endregion

        public ModelMessage(short AddressSender, short AddressReceiver, byte CodeMessage, byte SignConfirm, int Length)
        {
            this.AddressReceiver = AddressReceiver;
            this.AddressSender = AddressSender;
            this.CodeMessage = CodeMessage;
            this.SignMessage = SignConfirm;
            this.ReservePart = ReservePart;
            this.Length = Length;
        }

        public ModelMessage()
        { }

        public static ModelMessage TryParseServicePart(byte[] data)
        {
            if (data== null || data.Length < LengthServicePart) return null;
            try
            {
                ModelMessage model = new ModelMessage();
                model.AddressSender= BitConverter.ToInt16(data, 0);
                model.AddressReceiver = BitConverter.ToInt16(data, 2);
                model.CodeMessage = data[4];
                model.SignMessage = data[5];
                model.ReservePart = data[6];
                model.Length = BitConverter.ToInt32(data, 7);
                return model;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static byte[] GetBitesServicePart(IServicePart data)
        {
            if (data == null) return new byte[] { };
            try
            {
                byte[] bytesServicePart = new byte[LengthServicePart];
                var bSender = BitConverter.GetBytes(data.AddressSender);
                var bReceiver = BitConverter.GetBytes(data.AddressReceiver);
                var bLength = BitConverter.GetBytes(data.Length);
                bytesServicePart[0] = bSender[0];
                bytesServicePart[1] = bSender[1];
                bytesServicePart[2] = bReceiver[0];
                bytesServicePart[3] = bReceiver[1];
                bytesServicePart[4] = data.CodeMessage;
                bytesServicePart[5] = data.SignMessage;
                bytesServicePart[6] = data.ReservePart;
                bytesServicePart[7] = bLength[0];
                bytesServicePart[8] = bLength[1];
                bytesServicePart[9] = bLength[2];
                bytesServicePart[10] = bLength[3];
                return bytesServicePart;
            }
            catch (Exception)
            {
                return new byte[] { };
            }
        }

    }
}
