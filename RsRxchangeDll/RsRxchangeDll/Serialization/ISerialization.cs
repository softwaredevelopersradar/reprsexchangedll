﻿using System.Collections.Generic;

namespace RsRxchangeDll.Serialization
{
    public interface ISerialization
    {
        byte[] Serialize<T>(T Model) where T : class;

        T Deserialize<T>(byte[] SerialModels) where T : class;

    }
}
