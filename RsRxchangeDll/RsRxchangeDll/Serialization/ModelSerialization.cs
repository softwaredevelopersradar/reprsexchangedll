﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RsRxchangeDll.Serialization
{
    public class ModelSerialization : ISerialization
    {
        public T Deserialize<T>(byte[] SerialModel) where T : class
        {
            try
            {
                //if (SerialModel.Length == 0)
                   // throw new Exception("Empty massive!");

                using (var stream = new MemoryStream(SerialModel))
                {
                    return Serializer.Deserialize<T>(stream);
                }
            }
            catch
            {
                throw;
            }
        }
        
        public byte[] Serialize<T>(T Model) where T : class
        {
            try
            {
                if (Model == null)
                    throw new Exception("Empty model!");
                byte[] result;
                using (var stream = new MemoryStream())
                {
                    Serializer.Serialize(stream, Model);
                    result = stream.ToArray();
                }
                return result;
            }
            catch
            {
                throw;
            }
        }

    }
}
