﻿namespace RsRxchangeDll
{
    internal interface IDeserializeMess
    {
        object TryDeserializeRequest(byte[] message);
    
        object TryDeserializeConfirm(byte[] message);
    }
}
