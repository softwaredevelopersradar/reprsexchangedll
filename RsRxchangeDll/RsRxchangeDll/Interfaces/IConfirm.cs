﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RsRxchangeDll
{
    [ProtoContract]
    [ProtoInclude(2, typeof(ModelConfirm<List<ModelASP>>))]
    [ProtoInclude(3, typeof(ModelConfirm<ModelSynchronize>))]
    [ProtoInclude(4, typeof(ModelConfirm<List<ModelReconFWS>>))]
    [ProtoInclude(5, typeof(ModelConfirm<List<ModelTempSuppr>>))]
    [ProtoInclude(6, typeof(ModelConfirm<ModelQuasiConfirm>))]
    [ProtoInclude(7, typeof(IConfirm))]

    [ProtoInclude(8, typeof(IConfirm<List<ModelASP>>))]
    [ProtoInclude(9, typeof(IConfirm<ModelSynchronize>))]
    [ProtoInclude(10, typeof(IConfirm<List<ModelReconFWS>>))]
    [ProtoInclude(11, typeof(IConfirm<List<ModelTempSuppr>>))]
    [ProtoInclude(12, typeof(IConfirm<ModelQuasiConfirm>))]
    public interface IConfirm
    {
        [ProtoMember(1)]
        byte CodeError { get; }
    }
}
