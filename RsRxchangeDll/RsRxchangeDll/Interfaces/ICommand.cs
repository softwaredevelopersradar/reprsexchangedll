﻿namespace RsRxchangeDll
{
    public interface ICommands
    {
        void SendRequest<T>(byte AddresSender, byte AddresReceiver, T model);

        void SendRequest(byte AddresSender, byte AddresReceiver);
        
        void SendConfirm(byte AddressSender, byte AddressReceiver, ModelConfirm CodeError);

    }

}
