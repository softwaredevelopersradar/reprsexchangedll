﻿using System;

namespace RsRxchangeDll
{
    internal interface IComOperation
    {
        bool OpenPort(string ComPortName, int speed, System.IO.Ports.Parity parity, int dataBits, System.IO.Ports.StopBits stopBits);

        bool OpenPort(string ComPortName, int speed);

        bool OpenPort(string ComPortName);

        void ClosePort();

        void WriteData(byte[] data);
        
        event EventHandler OnDisconnect;

        event EventHandler<ModelMessage> OnReadMessage;
    }
}
