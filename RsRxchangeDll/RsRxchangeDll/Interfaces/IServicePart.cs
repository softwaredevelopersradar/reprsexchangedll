﻿namespace RsRxchangeDll
{
    internal interface IServicePart
    {
        short AddressSender { get; }

        short AddressReceiver { get; }

        byte SignMessage { get; }

        byte CodeMessage { get; }

        byte ReservePart { get; }

        int Length { get; }
    }
}
