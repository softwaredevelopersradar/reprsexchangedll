﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RsRxchangeDll
{
    public interface IClickOn
    {
        void ClickOnRequest(object model);

        void ClickOnConfirm(object model);
    }
}
