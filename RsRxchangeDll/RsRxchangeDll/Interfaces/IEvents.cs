﻿using System;

namespace RsRxchangeDll
{
    public interface IEventRequest<T> //where T : class
    {
        event EventHandler<T> OnRequest;
    }

    public interface IEventConfirm
    {
        event EventHandler<ModelConfirm> OnConfirm;
    }  


}
