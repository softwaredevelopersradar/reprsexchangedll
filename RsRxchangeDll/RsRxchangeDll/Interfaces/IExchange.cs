﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RsRxchangeDll
{
    public interface IExchange
    {
        event EventHandler OnConnect;

        event EventHandler OnDisconnect;

        void Connect(string ComPortName);

        void Disconnect();
    }
}
