﻿namespace RsRxchangeDll
{
    public interface IQuasiConfirm
    {
        int StationId { get; set; }
        short Bearing { get; set; }
    }
}
