﻿using System;

namespace RsRxchangeDll
{
    public interface ISynchronize: IMessage
    {
        DateTime Time { get; set; }
    }
}
