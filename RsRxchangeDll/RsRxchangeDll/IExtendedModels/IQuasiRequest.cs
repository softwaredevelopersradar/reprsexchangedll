﻿using System;

namespace RsRxchangeDll
{
    public interface IQuasiRequest
    {
        int Frequency { get; set; }

        DateTime Time { get; set; }
    }
}
