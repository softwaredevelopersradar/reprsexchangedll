﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RsRxchangeDll
{
    public class ModelRequest<T> where T:class
    {
        public T Model { get; private set; }

        public ModelRequest( T model)
        {
            Model = model;
        }
    }
}
