﻿using ProtoBuf;

namespace RsRxchangeDll
{
    [ProtoContract]
    public class ModelConfirm<T> : ModelConfirm, IConfirm<T> //where T: class
    {
        [ProtoMember(1)]
        public T Model { get; set; }
    }
}
