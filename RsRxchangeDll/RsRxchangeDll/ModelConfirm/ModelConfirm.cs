﻿using ModelsTablesDBLib;
using ProtoBuf;
using System.Collections.Generic;
using ModelsTablesDBLib.Interfaces;

namespace RsRxchangeDll
{
    [ProtoContract]
    public class ModelConfirm: IConfirm
    {
        [ProtoMember(1)]
        public byte CodeError { get; set; }

    }
}
