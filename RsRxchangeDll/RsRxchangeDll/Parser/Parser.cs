﻿using System;
using System.Linq;
using RsRxchangeDll.Serialization;

namespace RsRxchangeDll
{
    public class Parser
    {
        #region Protected

        static protected ISerialization Serializer;

        internal Requests TypeRequest { get;  set; }

        public static event EventHandler<byte[]> OnSerializedModelMess;

        #endregion

        protected void SendSerializedData(byte[] data)
        {
            OnSerializedModelMess(this, data);
        }
        
        public void SendRequest(byte AddresSender, byte AddresReceiver)
        {
            try
            {
                if (Serializer != null)
                {
                    ModelMessage message = new ModelMessage(AddresSender, AddresReceiver, Common.Codes.FirstOrDefault(x => x.Value == TypeRequest).Key, ModelMessage.SignRequest,0);
                    var dataService = ModelMessage.GetBitesServicePart(message);
                    SendSerializedData(dataService);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Parser()
        { }

        public Parser(ISerialization Serializer)
        {
            Parser.Serializer = Serializer;
        }
    }
}
