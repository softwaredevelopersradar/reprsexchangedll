﻿using System;
using System.Linq;

namespace RsRxchangeDll
{
    public class ParserSynchronize : Parser, IDeserializeMess, ICommands, IEventRequest<DateTime>, IEventConfirm 
    {

        public event EventHandler<DateTime> OnRequest = (sender, model) => { };

        public event EventHandler<ModelConfirm> OnConfirm = (sender, model) => { };

        public object TryDeserializeConfirm(byte[] message)
        {
            try
            {
                if (Serializer != null)
                {
                    var confirm = Serializer.Deserialize<ModelConfirm<DateTime>>(message);
                    OnConfirm(this, confirm);
                    return confirm;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public object TryDeserializeRequest(byte[] message)
        {
            try
            {
                if (Serializer != null)
                {
                    var request = Serializer.Deserialize<ModelSynchronize>(message);
                    OnRequest(this, request.Time);
                    return request;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ParserSynchronize() : base()
        {
            TypeRequest = Requests.Synchronize;
        }

        public void SendRequest<T1>(byte AddresSender, byte AddresReceiver, T1 model)
        {
            try
            {
                if (Serializer != null)
                {
                    ModelSynchronize request = new ModelSynchronize()
                    {
                        Time = (DateTime)(model as object)
                    };
                    var dataModel = Serializer.Serialize(request);
                    if (dataModel == null)
                        dataModel = new byte[] { };
                    ModelMessage message = new ModelMessage(AddresSender, AddresReceiver, Common.Codes.FirstOrDefault(x => x.Value == TypeRequest).Key, ModelMessage.SignRequest, dataModel.Length);
                    var dataService = ModelMessage.GetBitesServicePart(message);
                    var data = dataService.Concat(dataModel).ToArray();
                    SendSerializedData(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendConfirm(byte AddresSender, byte AddresReceiver, ModelConfirm model)
        {
            try
            {
                if (Serializer != null)
                {
                    ModelConfirm<DateTime> confirm = new ModelConfirm<DateTime>()
                    {
                        CodeError = model.CodeError,
                        Model = (model as ModelConfirm<DateTime>).Model
                    };
                    var dataModel = Serializer.Serialize(confirm);
                    if (dataModel == null)
                        dataModel = new byte[] { };
                    ModelMessage message = new ModelMessage(AddresSender, AddresReceiver, Common.Codes.FirstOrDefault(x => x.Value == TypeRequest).Key, ModelMessage.SignConfirm, dataModel.Length);
                    var dataService = ModelMessage.GetBitesServicePart(message);
                    var data = dataService.Concat(dataModel).ToArray();
                    SendSerializedData(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
