﻿using System;
using System.Linq;

namespace RsRxchangeDll
{
    public class ParserMessages<T,I>: Parser, IDeserializeMess, ICommands, IEventRequest<I>, IEventConfirm where T : class,new() where I:class
    {
        public event EventHandler<I> OnRequest = (sender, model) => { };

        public event EventHandler<ModelConfirm> OnConfirm = (sender, model) => { };

        public object TryDeserializeConfirm(byte[] message)
        {
            try
            {
                if (Serializer != null)
                {
                    var confirm = Serializer.Deserialize<ModelConfirm>(message);
                    OnConfirm(this, new ModelConfirm<I>() { CodeError = confirm.CodeError});
                    return confirm;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public virtual object TryDeserializeRequest(byte[] message)
        {
            try
            {
                if (Serializer != null)
                {
                    var request = Serializer.Deserialize<T>(message);
                    OnRequest(this, request as I);
                    return request;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ParserMessages(Requests request):base()
        {
            TypeRequest = request;
        }

        public virtual void SendRequest<T1>(byte AddresSender, byte AddresReceiver, T1 model)
        {
            try
            {
                if (Serializer != null)
                {
                    T request = new T();
                    var propertiesT = request.GetType().GetProperties();
                    foreach (var prop in propertiesT)
                    {
                        var temp = model.GetType().GetProperties().FirstOrDefault(rec => (rec.Name == prop.Name) && (rec.GetType() == prop.GetType()));
                        if (temp != null)
                        {

                            prop.SetValue(request, temp.GetValue(model));
                        }
                    }
                    var dataModel = Serializer.Serialize(request);
                    if (dataModel == null)
                        dataModel = new byte[] { };
                    ModelMessage message = new ModelMessage(AddresSender, AddresReceiver, Common.Codes.FirstOrDefault(x => x.Value == TypeRequest).Key, ModelMessage.SignRequest, dataModel.Length);
                    var dataService = ModelMessage.GetBitesServicePart(message);
                    var data = dataService.Concat(dataModel).ToArray();
                    SendSerializedData(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendConfirm(byte AddresSender, byte AddresReceiver, ModelConfirm model)
        {
            try
            {
                if (Serializer != null)
                {
                    ModelConfirm confirm = new ModelConfirm()
                    {
                        CodeError = model.CodeError
                    };
                    var dataModel = Serializer.Serialize(confirm);
                    if (dataModel == null)
                        dataModel = new byte[] { };
                    ModelMessage message = new ModelMessage(AddresSender, AddresReceiver, Common.Codes.FirstOrDefault(x => x.Value == TypeRequest).Key, ModelMessage.SignConfirm, dataModel.Length);
                    var dataService = ModelMessage.GetBitesServicePart(message);
                    var data = dataService.Concat(dataModel).ToArray();
                    SendSerializedData(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public void SendRequest(byte AddresSender, byte AddresReceiver, I model)
        //{
        //    try
        //    {
        //        if (Serializer != null)
        //        {
        //            T request = new T();
        //            var propertiesT = request.GetType().GetProperties();
        //            foreach (var prop in propertiesT)
        //            {
        //                var temp = model.GetType().GetProperties().FirstOrDefault(rec => (rec.Name == prop.Name) && (rec.GetType() == prop.GetType()));
        //                if (temp != null)
        //                {

        //                    prop.SetValue(request, temp.GetValue(model));
        //                }
        //            }
        //            var dataModel = Serializer.Serialize(request);
        //            if (dataModel == null)
        //                dataModel = new byte[] { };
        //            ModelMessage message = new ModelMessage(AddresSender, AddresReceiver, Common.Codes.FirstOrDefault(x => x.Value == TypeRequest).Key, ModelMessage.SignRequest, dataModel.Length);
        //            var dataService = ModelMessage.GetBitesServicePart(message);
        //            var data = dataService.Concat(dataModel).ToArray();
        //            SendSerializedData(data);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}
