﻿using System;
using System.Linq;

namespace RsRxchangeDll
{
    public class ParserTextMessage : Parser, IDeserializeMess, ICommands, IEventRequest<string>, IEventConfirm
    {
        public event EventHandler<ModelConfirm> OnConfirm;
        public event EventHandler<string> OnRequest;

        public ParserTextMessage()
        {
            TypeRequest = Requests.TextMess;
        }

        public object TryDeserializeConfirm(byte[] message)
        {
            try
            {
                if (Serializer != null)
                {
                    var confirm = Serializer.Deserialize<ModelConfirm>(message);

                    OnConfirm(this, confirm);
                    return confirm;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public object TryDeserializeRequest(byte[] message)
        {
            try
            {
                if (Serializer != null)
                {
                    var request = Serializer.Deserialize<ModelTextMess>(message);
                    OnRequest(this, request.Message);
                    return request;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //public void SendRequest(byte AddresSender, byte AddresReceiver, object model)
        //{
        //    try
        //    {
        //        if (Serializer != null)
        //        {
        //            ModelTextMess request = new ModelTextMess()
        //            {
        //                Message = ((ITextMessage)model).Message
        //            };
        //            var dataModel = Serializer.Serialize(request);
        //            if (dataModel == null)
        //                dataModel = new byte[] { };
        //            ModelMessage message = new ModelMessage(AddresSender, AddresReceiver, Common.Codes.FirstOrDefault(x => x.Value == TypeRequest).Key, ModelMessage.SignRequest, dataModel.Length);
        //            var dataService = ModelMessage.GetBitesServicePart(message);
        //            var data = dataService.Concat(dataModel).ToArray();
        //            SendSerializedData(data);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public void SendConfirm(byte AddresSender, byte AddresReceiver, ModelConfirm model)
        {
            try
            {
                if (Serializer != null)
                {
                    ModelConfirm confirm = new ModelConfirm()
                    {
                        CodeError = model.CodeError
                    };
                    var dataModel = Serializer.Serialize(confirm);
                    if (dataModel == null)
                        dataModel = new byte[] { };
                    ModelMessage message = new ModelMessage(AddresSender, AddresReceiver, Common.Codes.FirstOrDefault(x => x.Value == TypeRequest).Key, ModelMessage.SignConfirm, dataModel.Length);
                    var dataService = ModelMessage.GetBitesServicePart(message);
                    var data = dataService.Concat(dataModel).ToArray();
                    SendSerializedData(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendRequest<T1>(byte AddresSender, byte AddresReceiver, T1 model)
        {
            try
            {
                if (Serializer != null)
                {
                    ModelTextMess request = new ModelTextMess()
                    {
                        Message = (string)(model as object)
                    };
                    var dataModel = Serializer.Serialize(request);
                    if (dataModel == null)
                        dataModel = new byte[] { };
                    ModelMessage message = new ModelMessage(AddresSender, AddresReceiver, Common.Codes.FirstOrDefault(x => x.Value == TypeRequest).Key, ModelMessage.SignRequest, dataModel.Length);
                    var dataService = ModelMessage.GetBitesServicePart(message);
                    var data = dataService.Concat(dataModel).ToArray();
                    SendSerializedData(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
