﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace RsRxchangeDll
{
    public class ParserListMessages<T, I> : Parser, IDeserializeMess, ICommands, IEventRequest<List<I>>, IEventConfirm where T : class, new() where I : class
    {
        public event EventHandler<List<I>> OnRequest = (sender, model) => { };
        
        public event EventHandler<ModelConfirm> OnConfirm = (sender, model) => { };

        public object TryDeserializeConfirm(byte[] message)
        {
            try
            {
                if (Serializer != null)
                {
                    var confirm = Serializer.Deserialize<ModelConfirm<List<T>>>(message);
                    if (confirm != null)
                    {
                        ModelConfirm<List<I>> modelConfirm;
                        modelConfirm = new ModelConfirm<List<I>>()
                        {
                            CodeError = confirm.CodeError,
                            Model = confirm.Model?.Select(rec => rec as I).ToList()
                        };
                        OnConfirm(this, modelConfirm);
                    }
                    return confirm;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public object TryDeserializeRequest(byte[] message)
        {
            try
            {
                if (Serializer != null)
                {
                    var request = Serializer.Deserialize<List<T>>(message);
                    OnRequest(this, request.Select(rec => rec as I).ToList());
                    return request;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ParserListMessages(Requests request) : base()
        {
            TypeRequest = request;
        }

        //public void SendRequest(byte AddresSender, byte AddresReceiver, object model)
        //{
        //    try
        //    {
        //        if (Serializer != null && model is IList)
        //        {
        //            List<T> request = new List<T>();
        //            foreach (var record in (model as IEnumerable))
        //            {
        //                T itemValue = new T();

        //                foreach (var prop in record.GetType().GetProperties())
        //                {
        //                    var temp = itemValue.GetType().GetProperties().FirstOrDefault(rec => (rec.Name == prop.Name) && (rec.GetType() == prop.GetType()));
        //                    if (temp != null)
        //                    {
        //                        temp.SetValue(itemValue, prop.GetValue(record));
        //                    }
        //                }
        //                if (itemValue != default(T))
        //                    request.Add(itemValue);
        //            }
        //            var dataModel = Serializer.Serialize(request);
        //            if (dataModel == null)
        //                dataModel = new byte[] { };
        //            ModelMessage message = new ModelMessage(AddresSender, AddresReceiver, Common.Codes.FirstOrDefault(x => x.Value == TypeRequest).Key, ModelMessage.SignRequest, dataModel.Length);
        //            var dataService = ModelMessage.GetBitesServicePart(message);
        //            var data = dataService.Concat(dataModel).ToArray();
        //            SendSerializedData(data);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void SendConfirm(byte AddresSender, byte AddresReceiver, ModelConfirm model)
        //{
        //    try
        //    {
        //        if (Serializer != null)
        //        {
        //            byte[] dataModel;
        //            var conatinModel = model.GetType().GetProperties().FirstOrDefault(rec => rec.Name == nameof(IConfirm<I>.Model));
        //            if (conatinModel != null)
        //            {
        //                var Model = conatinModel.GetValue(model);
        //                ModelConfirm<List<T>> confirm = new ModelConfirm<List<T>>()
        //                {
        //                    CodeError = model.CodeError,
        //                    Model = new List<T>()
        //                };
        //                foreach (var item in (Model as IEnumerable))
        //                {
        //                    T itemConfirm = new T();
        //                    foreach (var prop in item.GetType().GetProperties())
        //                    {
        //                        var temp = itemConfirm.GetType().GetProperties().FirstOrDefault(rec => (rec.Name == prop.Name) && (rec.GetType() == prop.GetType()));
        //                        if (temp != null)
        //                        {
        //                            temp.SetValue(itemConfirm, prop.GetValue(item));
        //                        }
        //                    }
        //                    if (itemConfirm != default(T))
        //                        confirm.Model.Add(itemConfirm);
        //                }
        //                dataModel = Serializer.Serialize(confirm);
        //            }
        //            else
        //            {
        //                ModelConfirm confirm = new ModelConfirm()
        //                {
        //                    CodeError = model.CodeError
        //                };
        //                dataModel = Serializer.Serialize(confirm);
        //            }
        //            if (dataModel == null)
        //                dataModel = new byte[] { };
        //            ModelMessage message = new ModelMessage(AddresSender, AddresReceiver, Common.Codes.FirstOrDefault(x => x.Value == TypeRequest).Key, ModelMessage.SignConfirm, dataModel.Length);
        //            var dataService = ModelMessage.GetBitesServicePart(message);
        //            var data = dataService.Concat(dataModel).ToArray();
        //            SendSerializedData(data);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public void SendRequest<T1>(byte AddresSender, byte AddresReceiver, T1 model)
        {
            try
            {
                if (Serializer != null && model is IList)
                {
                    List<T> request = new List<T>();
                    foreach (var record in (model as IEnumerable))
                    {
                        T itemValue = new T();

                        foreach (var prop in record.GetType().GetProperties())
                        {
                            var temp = itemValue.GetType().GetProperties().FirstOrDefault(rec => (rec.Name == prop.Name) && (rec.GetType() == prop.GetType()));
                            if (temp != null)
                            {
                                temp.SetValue(itemValue, prop.GetValue(record));
                            }
                        }
                        if (itemValue != default(T))
                            request.Add(itemValue);
                    }
                    var dataModel = Serializer.Serialize(request);
                    if (dataModel == null)
                        dataModel = new byte[] { };
                    ModelMessage message = new ModelMessage(AddresSender, AddresReceiver, Common.Codes.FirstOrDefault(x => x.Value == TypeRequest).Key, ModelMessage.SignRequest, dataModel.Length);
                    var dataService = ModelMessage.GetBitesServicePart(message);
                    var data = dataService.Concat(dataModel).ToArray();
                    SendSerializedData(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendConfirm(byte AddresSender, byte AddresReceiver, ModelConfirm model)
        {
            try
            {
                if (Serializer != null)
                {
                    byte[] dataModel;
                    var conatinModel = model.GetType().GetProperties().FirstOrDefault(rec => rec.Name == nameof(IConfirm<I>.Model));
                    if (conatinModel != null)
                    {
                        var Model = conatinModel.GetValue(model);
                        ModelConfirm<List<T>> confirm = new ModelConfirm<List<T>>()
                        {
                            CodeError = model.CodeError,
                            Model = new List<T>()
                        };
                        foreach (var item in (Model as IEnumerable))
                        {
                            T itemConfirm = new T();
                            foreach (var prop in item.GetType().GetProperties())
                            {
                                var temp = itemConfirm.GetType().GetProperties().FirstOrDefault(rec => (rec.Name == prop.Name) && (rec.GetType() == prop.GetType()));
                                if (temp != null)
                                {
                                    temp.SetValue(itemConfirm, prop.GetValue(item));
                                }
                            }
                            if (itemConfirm != default(T))
                                confirm.Model.Add(itemConfirm);
                        }
                        dataModel = Serializer.Serialize(confirm);
                    }
                    else
                    {
                        ModelConfirm confirm = new ModelConfirm()
                        {
                            CodeError = model.CodeError
                        };
                        dataModel = Serializer.Serialize(confirm);
                    }
                    if (dataModel == null)
                        dataModel = new byte[] { };
                    ModelMessage message = new ModelMessage(AddresSender, AddresReceiver, Common.Codes.FirstOrDefault(x => x.Value == TypeRequest).Key, ModelMessage.SignConfirm, dataModel.Length);
                    var dataService = ModelMessage.GetBitesServicePart(message);
                    var data = dataService.Concat(dataModel).ToArray();
                    SendSerializedData(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
