﻿using System;
using System.Linq;

namespace RsRxchangeDll
{
    public class ParserQuasi : Parser, IDeserializeMess, ICommands, IEventRequest<IQuasiRequest>, IEventConfirm
    {
        public event EventHandler<ModelConfirm> OnConfirm;

        public event EventHandler<IQuasiRequest> OnRequest;


        public object TryDeserializeConfirm(byte[] message)
        {
            try
            {
                if (Serializer != null)
                {
                    var confirm = Serializer.Deserialize<ModelConfirm<ModelQuasiConfirm>>(message);
                    ModelConfirm<IQuasiConfirm> confirmQuasi = new ModelConfirm<IQuasiConfirm>
                    {
                        CodeError = confirm.CodeError,
                        Model = confirm.Model
                    };
                    OnConfirm(this, confirmQuasi);
                    return confirm;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public object TryDeserializeRequest(byte[] message)
        {
            try
            {
                if (Serializer != null)
                {
                    var request = Serializer.Deserialize<ModelQuasiRequest>(message);
                    OnRequest(this, request);
                    return request;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ParserQuasi() : base()
        {
            TypeRequest = Requests.QuasiDirectFind;
        }

        public void SendRequest<T1>(byte AddresSender, byte AddresReceiver, T1 model) 
        {
            try
            {
                if (Serializer != null)
                {
                    ModelQuasiRequest request = new ModelQuasiRequest()
                    {
                        Frequency = (model as IQuasiRequest).Frequency,
                        Time = (model as IQuasiRequest).Time
                    };
                    var dataModel = Serializer.Serialize(request);
                    if (dataModel == null)
                        dataModel = new byte[] { };
                    ModelMessage message = new ModelMessage(AddresSender, AddresReceiver, Common.Codes.FirstOrDefault(x => x.Value == TypeRequest).Key, ModelMessage.SignRequest, dataModel.Length);
                    var dataService = ModelMessage.GetBitesServicePart(message);
                    var data = dataService.Concat(dataModel).ToArray();
                    SendSerializedData(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendConfirm(byte AddresSender, byte AddresReceiver, ModelConfirm model)
        {
            try
            {
                if (Serializer != null)
                {
                    ModelConfirm<ModelQuasiConfirm> confirm = new ModelConfirm<ModelQuasiConfirm>()
                    {
                        CodeError = model.CodeError,
                        Model = new ModelQuasiConfirm()
                        {
                            Bearing = (model as IConfirm<IQuasiConfirm>).Model.Bearing,
                            StationId = (model as IConfirm<IQuasiConfirm>).Model.StationId
                        }
                    };
                    var dataModel = Serializer.Serialize(confirm);
                    if (dataModel == null)
                        dataModel = new byte[] { };
                    ModelMessage message = new ModelMessage(AddresSender, AddresReceiver, Common.Codes.FirstOrDefault(x => x.Value == TypeRequest).Key, ModelMessage.SignConfirm, dataModel.Length);
                    var dataService = ModelMessage.GetBitesServicePart(message);
                    var data = dataService.Concat(dataModel).ToArray();
                    SendSerializedData(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}