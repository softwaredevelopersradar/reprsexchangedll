﻿using System;

namespace RsRxchangeDll
{
    [ProtoBuf.ProtoContract]
    internal class ModelQuasiRequest: IMessage, IQuasiRequest
    {
        [ProtoBuf.ProtoMember(1)]
        public int Frequency { get; set; }

        [ProtoBuf.ProtoMember(2)]
        public DateTime Time { get; set; }

    }
}
