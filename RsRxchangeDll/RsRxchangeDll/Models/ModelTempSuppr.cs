﻿using ModelsTablesDBLib;
using ModelsTablesDBLib.Interfaces;

namespace RsRxchangeDll
{
    [ProtoBuf.ProtoContract]
    internal class ModelTempSuppr : ITempSuppr
    {
        [ProtoBuf.ProtoMember(1)]
        public int Id { get; set; }

        [ProtoBuf.ProtoMember(2)]
        public Led Control { get; set; }

        [ProtoBuf.ProtoMember(3)]
        public Led Suppress { get; set; }

        [ProtoBuf.ProtoMember(4)]
        public Led Radiation { get; set; }

        public ModelTempSuppr()
        {

        }

            public ModelTempSuppr(IFixTempSuppr supprFHSS)
        {
            Id = supprFHSS.Id;
            Control = supprFHSS.Control;
            Radiation = supprFHSS.Radiation;
            Suppress = supprFHSS.Suppress;
        }
    }
}
