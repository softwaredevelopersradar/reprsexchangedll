﻿using ModelsTablesDBLib;
using ModelsTablesDBLib.Interfaces;

namespace RsRxchangeDll
{
    [ProtoBuf.ProtoContract]
    internal class ModelASP : IASP
    {
        [ProtoBuf.ProtoMember(1)]
        public int Id { get; set; }

        [ProtoBuf.ProtoMember(2)]
        public int IdMission { get; set; }

        [ProtoBuf.ProtoMember(3)]
        public byte Mode { get; set; }

        [ProtoBuf.ProtoMember(4)]
        public byte[] Letters { get; set; }

        [ProtoBuf.ProtoMember(5)]
        public short RRS1 { get; set; }

        [ProtoBuf.ProtoMember(6)]
        public short RRS2 { get; set; }

        [ProtoBuf.ProtoMember(7)]
        public short LPA13 { get; set; }

        [ProtoBuf.ProtoMember(8)]
        public short LPA24 { get; set; }

        [ProtoBuf.ProtoMember(9)]
        public short LPA510 { get; set; }

        [ProtoBuf.ProtoMember(10)]
        public Coord Coordinates { get; set; }

        [ProtoBuf.ProtoMember(11)]
        public double Latitude
        {
            get => Coordinates.Latitude;
            set
            {
                Coordinates.Latitude = value;
            }
        }

        [ProtoBuf.ProtoMember(12)]
        public double Longitude
        {
            get => Coordinates.Longitude;
            set
            {
                Coordinates.Longitude = value;
            }
        }

        [ProtoBuf.ProtoMember(13)]
        public double Altitude
        {
            get => Coordinates.Altitude;
            set
            {
                Coordinates.Altitude = value;
            }
        }

        [ProtoBuf.ProtoMember(14)]
        public short BPSS { get; set; }

        [ProtoBuf.ProtoMember(15)]
        public short LPA57 { get; set; }

        [ProtoBuf.ProtoMember(16)]
        public short LPA59 { get; set; }

        [ProtoBuf.ProtoMember(17)]
        public short LPA10 { get; set; }

        public ModelASP()
        {

        }

        public ModelASP(IFixASP asp)
        {
            Id = asp.Id;
            IdMission = asp.IdMission;
            Coordinates = asp.Coordinates;
            Letters = asp.Letters;
            LPA13 = asp.LPA13;
            LPA24 = asp.LPA24;
            LPA510 = asp.LPA510;
            Mode = asp.Mode;
            RRS1 = asp.RRS1;
            RRS2 = asp.RRS2; 
            BPSS = asp.BPSS;
            LPA57 = asp.LPA57;
            LPA59 = asp.LPA59;
            LPA10 = asp.LPA10;
        }
    }
}
