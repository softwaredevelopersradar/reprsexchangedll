﻿using ModelsTablesDBLib;
using ModelsTablesDBLib.Interfaces;
namespace RsRxchangeDll
{
    [ProtoBuf.ProtoContract]
    internal class ModelSupprFHSS : ISupprFHSS
    {
        [ProtoBuf.ProtoMember(1)]
        public int Id { get; set; }

        [ProtoBuf.ProtoMember(2)]
        public double FreqMinKHz { get; set; }

        [ProtoBuf.ProtoMember(3)]
        public double FreqMaxKHz { get; set; }

        [ProtoBuf.ProtoMember(4)]
        public double Threshold { get; set; }

        [ProtoBuf.ProtoMember(5)]
        public short StepKHz { get; set; }

        [ProtoBuf.ProtoMember(6)]
        public InterferenceParam InterferenceParam { get; set; }

        [ProtoBuf.ProtoMember(7)]
        public byte[] Letters { get; set; }

        [ProtoBuf.ProtoMember(8)]
        public byte[] EPO { get; set; }
    }
}
