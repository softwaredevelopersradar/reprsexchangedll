﻿namespace RsRxchangeDll
{
    [ProtoBuf.ProtoContract]
    internal class ModelQuasiConfirm: IMessage, IQuasiConfirm
    {
        [ProtoBuf.ProtoMember(1)]
        public short Bearing { get; set; }
        [ProtoBuf.ProtoMember(2)]
        public int StationId { get; set; }
    }
}
