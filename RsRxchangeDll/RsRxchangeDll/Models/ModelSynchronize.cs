﻿using System;

namespace RsRxchangeDll
{
    [ProtoBuf.ProtoContract]
    internal class ModelSynchronize: ISynchronize
    {
        [ProtoBuf.ProtoMember(1)]
        public DateTime Time { get; set; }
    }
}
