﻿using System;
using System.Collections.ObjectModel;
using ModelsTablesDBLib;
using ModelsTablesDBLib.Interfaces;

namespace RsRxchangeDll
{
    [ProtoBuf.ProtoContract]
    internal class ModelReconFWS : IReconFWS
    {
        [ProtoBuf.ProtoMember(1)]
        public int Id { get; set; }

        [ProtoBuf.ProtoMember(2)]
        public double FreqKHz { get; set; }

        [ProtoBuf.ProtoMember(3)]
        public float Deviation { get; set; }

        [ProtoBuf.ProtoMember(4)]
        public Coord Coordinates { get; set; }

        [ProtoBuf.ProtoMember(5)]
        public ObservableCollection<TableJamDirect> ListJamDirect { get; set; }

        [ProtoBuf.ProtoMember(6)]
        public DateTime Time { get; set; }

        [ProtoBuf.ProtoMember(7)]
        public byte Type { get; set; }

        [ProtoBuf.ProtoMember(8)]
        public SignSender? Sender { get; set; }

        [ProtoBuf.ProtoMember(9)]
        public ModulationKondor Modulation { get; set; }

        public ModelReconFWS()
        {

        }

        public ModelReconFWS(IFixReconFWS reconFWS)
        {
            Id = reconFWS.Id;
            FreqKHz = reconFWS.FreqKHz;
            Deviation = reconFWS.Deviation;
            Coordinates = reconFWS.Coordinates;
            ListJamDirect = reconFWS.ListJamDirect;
            Time = reconFWS.Time;
            Type = reconFWS.Type;
            Sender = reconFWS.Sender;
            Modulation = reconFWS.Modulation;
        }
    }
}
