﻿using ModelsTablesDBLib.Interfaces;

namespace RsRxchangeDll
{
    [ProtoBuf.ProtoContract]
    internal class ModelSectorRanges : ISectorRanges
    {
        [ProtoBuf.ProtoMember(1)]
        public int Id { get; set; }

        [ProtoBuf.ProtoMember(2)]
        public double FreqMinKHz { get; set; }

        [ProtoBuf.ProtoMember(3)]
        public double FreqMaxKHz { get; set; }

        [ProtoBuf.ProtoMember(4)]
        public short AngleMin { get; set; }

        [ProtoBuf.ProtoMember(5)]
        public short AngleMax { get; set; }

        public ModelSectorRanges()
        {

        }

        public ModelSectorRanges(IFixSectorRanges sectorRanges)
        {
            Id = sectorRanges.Id;
            FreqMinKHz = sectorRanges.FreqMinKHz;
            FreqMaxKHz = sectorRanges.FreqMaxKHz;
            AngleMin = sectorRanges.AngleMin;
            AngleMax = sectorRanges.AngleMax;
        }
    }
}
