﻿namespace RsRxchangeDll
{
    [ProtoBuf.ProtoContract]
    internal class ModelRegime: IRegime
    {
        [ProtoBuf.ProtoMember(1)]
        public byte Regime { get; set; }
    }
}
