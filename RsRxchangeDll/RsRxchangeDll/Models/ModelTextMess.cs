﻿namespace RsRxchangeDll
{
    [ProtoBuf.ProtoContract]
    internal class ModelTextMess: ITextMessage
    {
        [ProtoBuf.ProtoMember(1)]
        public string Message { get; set; }
    }
}
