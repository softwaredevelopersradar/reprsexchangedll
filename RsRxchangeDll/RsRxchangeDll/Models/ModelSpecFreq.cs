﻿using ModelsTablesDBLib.Interfaces;

namespace RsRxchangeDll
{
    [ProtoBuf.ProtoContract]
    internal class ModelSpecFreq : ISpecFreq
    {
        [ProtoBuf.ProtoMember(1)]
        public int Id { get; set; }

        [ProtoBuf.ProtoMember(2)]
        public double FreqMinKHz { get; set; }

        [ProtoBuf.ProtoMember(3)]
        public double FreqMaxKHz { get; set; }

        public ModelSpecFreq()
        {

        }
            public ModelSpecFreq(IFixSpecFreq specFreq)
        {
            Id = specFreq.Id;
            FreqMinKHz = specFreq.FreqMinKHz;
            FreqMaxKHz = specFreq.FreqMaxKHz;
        }
    }
}
