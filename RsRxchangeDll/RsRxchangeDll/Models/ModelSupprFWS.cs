﻿using ModelsTablesDBLib;
using ModelsTablesDBLib.Interfaces;

namespace RsRxchangeDll
{
    [ProtoBuf.ProtoContract]
    internal class ModelSupprFWS : ISupprFWS
    {
        [ProtoBuf.ProtoMember(1)]
        public int Id { get; set; }

        [ProtoBuf.ProtoMember(2)]
        public Coord Coordinates { get; set; }

        [ProtoBuf.ProtoMember(3)]
        public double? FreqKHz { get; set; }

        [ProtoBuf.ProtoMember(4)]
        public float? Bearing { get; set; }

        [ProtoBuf.ProtoMember(5)]
        public byte? Letter { get; set; }

        [ProtoBuf.ProtoMember(6)]
        public short? Threshold { get; set; }

        [ProtoBuf.ProtoMember(7)]
        public byte? Priority { get; set; }

        [ProtoBuf.ProtoMember(8)]
        public InterferenceParam InterferenceParam { get; set; }

        [ProtoBuf.ProtoMember(9)]
        public byte Modulation
        {
            get => InterferenceParam.Modulation;
            set
            {
                InterferenceParam.Modulation = value;
            }
        }
        
        [ProtoBuf.ProtoMember(10)]
        public byte Deviation
        {
            get => InterferenceParam.Deviation;
            set
            {
                InterferenceParam.Deviation = value;
            }
        }
        
        [ProtoBuf.ProtoMember(11)]
        public byte Manipulation
        {
            get => InterferenceParam.Manipulation;
            set
            {
                InterferenceParam.Manipulation = value;
            }
        }
        
        [ProtoBuf.ProtoMember(12)]
        public byte Duration
        {
            get => InterferenceParam.Duration;
            set
            {
                InterferenceParam.Duration = value;
            }
        }

        public ModelSupprFWS()
        {

        }

            public ModelSupprFWS(IFixSupprFWS supprFWS)
        {
            Id = supprFWS.Id;
            FreqKHz = supprFWS.FreqKHz;
            Coordinates = supprFWS.Coordinates;
            Bearing = supprFWS.Bearing;
            InterferenceParam = supprFWS.InterferenceParam;
            Letter = supprFWS.Letter;
            Priority = supprFWS.Priority;
            Threshold = supprFWS.Threshold;
        }
    }
}
