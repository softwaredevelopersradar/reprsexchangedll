﻿using System;
using System.Collections.Generic;

namespace RsRxchangeDll
{
    public class QuasiReq : IQuasiRequest
    {
        public int Frequency { get; set; }

        public DateTime Time { get; set; }
    }

    public class QuasiConf : IQuasiConfirm
    {
        public short Bearing { get; set; }
        public int StationId { get; set; }
    }
    public static class Common
    {
        public static readonly Dictionary<byte, Requests> Codes = new Dictionary<byte, Requests>()
        {
            {100, Requests.TextMess },
            {101, Requests.TableAsp },
            {102, Requests.Synchronize },
            {103, Requests.TableSectorRangeRecon},
            {104, Requests.TableSectorRangeSuppr},
            {105, Requests.TableFreqForbidden},
            {106, Requests.TableFreqKnown},
            {107, Requests.TableFreqImportant},
            {108, Requests.TableSuppressFWS },
            {109, Requests.TableSuppressFHSS},
            {110, Requests.Regime },
            {111, Requests.TableReconFWS },
            {112, Requests.TempSuppressFWS},
            {113, Requests.TempSuppressFHSS },
            {114, Requests.QuasiDirectFind },
            {255, Requests.Ping }
        };
    }

    public enum Requests : byte
    {
        TextMess,
        TableAsp,
        Synchronize,
        TableSectorRangeRecon,
        TableSectorRangeSuppr,
        TableFreqForbidden,
        TableFreqKnown,
        TableFreqImportant,
        TableSuppressFWS,
        TableSuppressFHSS,
        Regime,
        TableReconFWS,
        TempSuppressFWS,
        TempSuppressFHSS,
        QuasiDirectFind,
        Ping = 255
    }
}
